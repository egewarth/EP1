#ifndef PACOTE_H
#define PACOTE_H

#include "array.hpp"

using namespace std;

class Pacote{
	private:
		array::array *pacote;
		byte tag;
		array::array *value;
	public:
		Pacote();
		Pacote(byte tag);
		Pacote(byte tag, array::array *value);
		Pacote(array::array *pacote, byte tag);
		//array::array *getPacote();
		//array::array *getValue();
		//byte getTag();
		//array::array *PegaPacote();
		array::array *getPacote();
		array::array *getValue();
};
#endif