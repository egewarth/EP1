#include "socket.hpp"

#include <sstream>
#include <ios>
#include <iomanip>

#include <iostream>

using namespace std;

#define READ 2
#define WRITE 3
#define HEADER_SIZE 4

/* #############################################
				struct union
############################################# */
typedef union sizeOfPackage{
	int size;
	byte byte_size[4];
}SizeOfPackage;

/* #############################################
				construtores
############################################# */
/*			construtor de pacote vazio		  */
Socket::Socket(int fd, array::array *package){
	array::array *header = array::create(HEADER_SIZE);
	SizeOfPackage sizeOfPackage;
	sizeOfPackage.size = package->length;
	header->data[0] = sizeOfPackage.byte_size[0];
	header->data[1] = sizeOfPackage.byte_size[1];
	header->data[2] = sizeOfPackage.byte_size[2];
	header->data[3] = sizeOfPackage.byte_size[3];
	network::write(fd, header);
	network::write(fd, package);
}
Socket::Socket(int fd, size_t length){
	array::array *header = array::create(HEADER_SIZE);
	array::array *package;
	SizeOfPackage sizeOfPackage;
	header = network::read(fd, HEADER_SIZE);
	if(network::read(fd, HEADER_SIZE) == nullptr){
		std::cout << "leitura nula" << std::endl;
	}else{
		sizeOfPackage.byte_size[0] = header->data[0];
		sizeOfPackage.byte_size[1] = header->data[1];
		sizeOfPackage.byte_size[2] = header->data[2];
		sizeOfPackage.byte_size[3] = header->data[3];
		package = network::read(fd, sizeOfPackage.size);
		setPackage(package);
	}	
}
/* #############################################
				inicio seters
############################################# */
void Socket::setPackage(array::array *package){
	this-> package = package;
	//this-> value = accessValue(package);	
}
/* #############################################
				inicio geters
############################################# */
array::array *Socket::getPackage(){
	return this->package;
}