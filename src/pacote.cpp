#include "pacote.hpp"
#include "crypto.hpp"
#include "array.hpp"

#include <iostream>

typedef union size_of_package{
	int size;
	byte byte_size[4];
}sizePackage;

typedef union size_of_package_interno{
	int size;
	byte byte_size[2];
}InternalSize;

/////////////////////////////////////CRIA PACOTE VAZIO//////////////////////////////////////////////////////////////////////
Pacote::Pacote(byte tag){
	//this->tag = tag;
	pacote = array::create(7);
	pacote -> data[0] = 0x03;
	pacote -> data[1] = 0;
	pacote -> data[2] = 0;
	pacote -> data[3] = 0;
	pacote -> data[4] = tag;
	pacote -> data[5] = 0;
	pacote -> data[6] = 0;
}

/////////////////////////////////////CRIA PACOTE COM CONTEUDO////////////////////////////////////////////////////////////////
Pacote::Pacote(byte tag, array::array *value){
	array::array *hash;
	pacote = array::create((int)value->length + 27);	
	hash = crypto::sha1(value);
	sizePackage size_of_package;
	InternalSize size_interno; 
	size_of_package.size = 23+(int)value->length;
	size_interno.size = (int)value->length;
	pacote->data[0] = size_of_package.byte_size[0];
	pacote->data[1] = size_of_package.byte_size[1];
	pacote->data[2] = size_of_package.byte_size[2];
	pacote->data[3] = size_of_package.byte_size[3];
	pacote->data[4] = tag;
	pacote->data[5] = size_interno.byte_size[0];
	pacote->data[6] = size_interno.byte_size[1];
	
	memcpy(pacote->data + 7, value->data, value->length);
	memcpy(pacote->data + 7 + value->length, hash->data, 20);
	
	/*cout << "imprimindo pacote" << endl;
	for(int j = 0;j < ((int) pacote->length);j++){
				printf("0x%X ", pacote->data[j]);
			}
			printf("\n\n\n\n\n");*/
}
/////////////////////////////////////RECEBE PACOTE E ACESSA O VALUE DELE, TESTA A TAG,E A HASH/////////////////////////////////
Pacote::Pacote(array::array *pacote_recebido, byte tag){
	byte tag_recebido;
	array::array *length_recebido = array::create(2);
	array::array *value_recebido;
	array::array *hash_recebido = array::create(20);
	//cout << "pacote dentro pacote:" << *pacote_recebido << endl;
	if(pacote_recebido->data[0] != tag){
		//cout << "tag diferente da esperada" << endl;
	}else{
		//cout << "entrou else" << endl;
		tag_recebido = pacote_recebido->data[0];
		length_recebido->data[0] = pacote_recebido->data[1];
		length_recebido->data[1] = pacote_recebido->data[2];
		InternalSize size_interno;
		size_interno.byte_size[0] = length_recebido->data[0];
		size_interno.byte_size[1] = length_recebido->data[1];

		//cout << "size value " << size_interno.size << endl;

		value_recebido = array::create(size_interno.size);
		for(int i = 3, j=0; i < (size_interno.size+3), j< size_interno.size;i++, j++){
			//cout << "i = " << i <<  endl;
			value_recebido->data[j] = pacote_recebido->data[i];
		}
		for(int i = size_interno.size, f = 0; i < (size_interno.size + 20), f < 20; i++, f++){
			hash_recebido->data[f] = pacote_recebido->data[i];
		}
		this->value = value_recebido;
	}
}

array::array *Pacote::getPacote(){
	return pacote;
}
array::array *Pacote::getValue(){
	return value;
}
