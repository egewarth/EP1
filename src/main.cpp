#include "crypto.hpp"
#include "array.hpp"
#include "network.hpp"
#include "pacote.hpp"
#include "socket.hpp"

#include <iostream>
#include <iomanip>
#include <ostream>
#include <istream>
#include <unistd.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <fstream>

using namespace std;

#define SERVER_ID "45.55.185.4"
#define SIZE_OF_HEADER 4

#define REQUEST_REGISTRATION 0xC0
#define REGISTRATION_START 0xC1
#define REGISTER_ID 0xC2
#define REGISTERED 0xC3

#define REQUEST_AUTH 0xA0 
#define AUTH_START 0xA1
#define REQUEST_CHALLENGE 0xA2
#define CHALLENGE 0xA4  
#define AUTHENTICATE 0xA5
#define AUTHENTICATED 0xA6

#define REQUEST_OBJECT 0xB0
#define OBJECT 0xB1

#define NOT_AUTHENTICATED 0xE0
#define OBJECT_NOT_FOUND 0xE1
#define INVALID_KEY 0xE2
#define ALREADY_REGISTERED 0xE3
#define FAILED_REGISTRATION 0xE4
#define NOT_AUTHORIZED 0xEF
#define SERVER_ERROR 0xE5

#define ACK 0xF0
#define NACK 0xF1
#define UNKNOWN 0xF2
#define END_COMMUNICATION 0xFF

/* #############################################
				struct union
############################################# */
typedef union size_of_package{
	int size;
	byte byte_size[4];
}SizePackage;

typedef union size_interno{
	int size;
	byte byte_size[2];
}InternalSize;

int main(int argc, char const *argv[]){
	int fd;
	/* #############################################
				declarando arrays
	############################################# */
	array::array *ID;
	array::array *object_ID;
	array::array *crypted_ID;
	array::array *package_registration_start;
	array::array *package_registered;
	array::array *package_auth_start;
	array::array *package_challenge;
	array::array *package_authenticated;
	array::array *package_object;
	array::array *hash;
	array::array *value_registered;
	array::array *token_a;
	array::array *key_s;
	array::array *package_m;
	array::array *header;
	array::array *token_t;
	array::array *decrypted_object;

	/* #############################################
				declarando objetos
	############################################# */

	Pacote *package_request_registration;
	Pacote *package_registration_start_auth;
	Pacote *package_register_id;
	Pacote *package_registered_auth;
	Pacote *package_request_auth;
	Pacote *package_token;
	Pacote *package_auth_start_auth;
	Pacote *package_request_challenge;
	Pacote *package_challenge_auth;
	Pacote *package_authenticate;
	Pacote *package_authenticated_auth;
	Pacote *package_request_object;
	Pacote *package_object_auth;

	/* #############################################
			declarando variaveis do tipo union
	############################################# */
	SizePackage package_registered_union;
	SizePackage package_auth_start_union;
	SizePackage package_challenge_union;
	SizePackage package_authenticated_union;
	SizePackage package_object_union;

	/* #############################################
				declarando chaves
	############################################# */
	
	RSA *private_key;
	RSA *server_public_key;

	/* #############################################
				definindo ID do ciente
	############################################# */
	
	ID = array::create(8);
	
	ID -> data[0] = 0xb8;
	ID -> data[1] = 0xf5;
	ID -> data[2] = 0x1b;
	ID -> data[3] = 0xe4;
	ID -> data[4] = 0x4b;
	ID -> data[5] = 0xdf;
	ID -> data[6] = 0x32;
	ID -> data[7] = 0x5b;
	
	/* #############################################
				definindo ID do objeto
	############################################# */
	
	object_ID = array::create(8);

	object_ID->data[0] = 0x01;     
	object_ID->data[1] = 0x02;     
	object_ID->data[2] = 0x03;     
	object_ID->data[3] = 0x04;     
	object_ID->data[4] = 0x05;     
	object_ID->data[5] = 0x06;     
	object_ID->data[6] = 0x07;     
	object_ID->data[7] = 0x08;     

	
	/* #############################################
				definindo chaves
	############################################# */
	
	private_key = crypto::rsa_read_private_key_from_PEM("bin/private.pem");
	server_public_key  = crypto::rsa_read_public_key_from_PEM("bin/server_pk.pem");

	crypted_ID = crypto::rsa_encrypt(ID, server_public_key);

	cout << "Starting communication with server\n" <<endl;

	fd = network::connect(SERVER_ID, 3000);
	
	if(fd < 0){
		cout << "Connection failed\n" << fd << endl;
	}else{
		cout << "Connection Ok\n" << endl;
		
		/* #############################################
				inicializando protocolo de registro
		############################################# */
	
		cout << "Starting Registration Protocol\n" << endl;
		/* #############################################
			tentando usar a classe socket >>>FALHOU<<<
		############################################# */
		//socket_request_registration = new Socket(fd, package_request_registration->getPacote());
		package_request_registration = new Pacote(REQUEST_REGISTRATION);
		network::write(fd, package_request_registration->getPacote());
		if((package_registration_start = network::read(fd))== nullptr){
			cout << "Error!! Unable to read the package\n" << endl;
		}else{
			package_registration_start_auth = new Pacote(package_registration_start, REGISTRATION_START);
			if(package_registration_start_auth->getPacote() == NULL){
				cout << "Error!! Different package than expected!\n" << endl; 
			}else{
				//nothing
			}
			
			package_register_id = new Pacote(REGISTER_ID, crypted_ID);
			network::write(fd, package_register_id->getPacote());
			
			SizePackage package_registered_union;
			if((header = network::read(fd, SIZE_OF_HEADER)) == nullptr){
				cout << "Error!! Unable to read the package\n" << endl;
			}else{
				package_registered_union.byte_size[0] = header->data[0];
				package_registered_union.byte_size[1] = header->data[1];
				package_registered_union.byte_size[2] = header->data[2];
				package_registered_union.byte_size[3] = header->data[3];
				if((package_registered = network::read(fd, package_registered_union.size)) == nullptr){
					cout << "Error!! Unable to read the package\n" << endl;	
				}else{
					package_registered_auth = new Pacote(package_registered, REGISTERED);
					array::array *chave_S = package_registered_auth->getValue();
					key_s = crypto::rsa_decrypt(chave_S, private_key);
					cout << "Registration Protocol Done!\n" << endl;
				}
			}
		}
		/* #############################################
			inicializando protocolo de autenticaçao
		############################################# */
		cout << "Starting Authentication Protocol\n" << endl;
		package_request_auth = new Pacote(REQUEST_AUTH, crypted_ID);
		network::write(fd, package_request_auth->getPacote());
		array::destroy(header);
		if((header = network::read(fd, SIZE_OF_HEADER)) == nullptr){
			cout << "Error!! Different package than expected!\n" << endl; 
		}else{
			package_auth_start_union.byte_size[0] = header->data[0];
			package_auth_start_union.byte_size[1] = header->data[1];
			package_auth_start_union.byte_size[2] = header->data[2];
			package_auth_start_union.byte_size[3] = header->data[3];
			if((package_auth_start = network::read(fd, package_auth_start_union.size)) == nullptr){
				cout << "Error!! Unable to read the package\n" << endl; 
			}else{
				package_auth_start_auth = new Pacote(package_auth_start, AUTH_START);
				array::array *token_A = package_auth_start_auth->getValue();
				token_a = crypto::rsa_decrypt(token_A, private_key);
			}
			package_request_challenge = new Pacote(REQUEST_CHALLENGE);
			network::write(fd, package_request_challenge->getPacote());

			array::destroy(header);
			if((header = network::read(fd, SIZE_OF_HEADER)) == nullptr){
				cout << "Error!! Unable to read the package\n" << endl;
			}else{
				package_challenge_union.byte_size[0] = header->data[0];
				package_challenge_union.byte_size[1] = header->data[1];
				package_challenge_union.byte_size[2] = header->data[2];
				package_challenge_union.byte_size[3] = header->data[3];
				if((package_challenge = network::read(fd, package_challenge_union.size)) == nullptr){
					cout << "Error!! Unable to read the package\n" << endl;	
				}else{
					package_challenge_auth = new Pacote(package_challenge, CHALLENGE);
					array::array *M_criptado = package_challenge_auth->getValue();
					package_m = crypto::aes_decrypt(M_criptado, token_a, key_s);
				}
			}
			package_authenticate = new Pacote(AUTHENTICATE, package_m);
			network::write(fd, package_authenticate->getPacote());
			array::destroy(header);
			if((header = network::read(fd, SIZE_OF_HEADER)) == nullptr){
				cout << "Error!! Unable to read the package\n" << endl;
			}else{
				package_authenticated_union.byte_size[0] = header->data[0];
				package_authenticated_union.byte_size[1] = header->data[1];
				package_authenticated_union.byte_size[2] = header->data[2];
				package_authenticated_union.byte_size[3] = header->data[3];
				if((package_authenticated = network::read(fd, package_authenticated_union.size)) == nullptr){
					cout << "Error!! Unable to read the package\n" << endl;	
				}else{
					package_authenticated_auth = new Pacote(package_authenticated, AUTHENTICATED);
					array::array *token_T = package_authenticated_auth->getValue();
					token_t = crypto::aes_decrypt(token_T, token_a, key_s);
					cout << "Authentication Protocol Done!\n" << endl;
				}
			}
			/* #############################################
				inicializando protocolo de requisiçao
			############################################# */
			cout << "Starting Request Protocol\n" << endl;
			array::array *object_crypted_ID = crypto::aes_encrypt(object_ID, token_t, key_s);
			package_request_object = new Pacote(REQUEST_OBJECT, object_crypted_ID);
			network::write(fd, package_request_object->getPacote());
			array::destroy(header);
			if((header = network::read(fd, SIZE_OF_HEADER)) == nullptr){
				cout << "Error!! Unable to read the package\n" << endl;
			}else{
				package_object_union.byte_size[0] = header->data[0];
				package_object_union.byte_size[1] = header->data[1];
				package_object_union.byte_size[2] = header->data[2];
				package_object_union.byte_size[3] = header->data[3];
				if((package_object = network::read(fd, package_object_union.size)) == nullptr){
					cout << "Error!! Unable to read the package\n" << endl;	
				}else{
					cout << "Starting Image Download\n" << endl;
					package_object_auth = new Pacote(package_object, OBJECT);
					array::array *objeto_encriptado = package_object_auth->getValue();

					decrypted_object = crypto::aes_decrypt(objeto_encriptado, token_t, key_s);
					cout << "Dowload completed\n" << endl;
					cout << "Request Protocol Done!\n" << endl;
				}
			}
		}
		/* #############################################
				salvando a imagem em jpg
		############################################# */
		ofstream imagem;
		imagem.open ("doc/imagem.jpg", ios::binary);
		if( !imagem ){
			cout << "Error!! Unable to read the package\n" << endl;
			return 1;
		}else{
			cout << "Creating the image\n" << endl;
			for (int i = 0; i < ((int)decrypted_object->length); i++){
				imagem << decrypted_object->data[i];
			}
		}
		imagem.close();

		cout << "DONE!\n" << endl;
		network::close(fd);
	}
	return 0;
}
